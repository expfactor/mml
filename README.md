MML - The Meta MacroProcessor
=============================

MML is a small macropreprocessor designe d to allow to "compile"
documents in different ways.

MML specifies are :
- It do multiple passes on the same file (Useful to compute TOC)
- It is written in Python and new tags can be defined using python
during processing
- It allow to specify explitly when blocks are evaluated
- It supports transductors and hence can emulate any grammar syntax.



--------------------------------
When you start using this project please install the  `git_hooks`:
`./scripts/install_git_hooks.sh`
