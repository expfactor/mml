"""
Meta Macro Language
"""
import os
from .__main__ import mml_main as __main__


try:
    __version__ = open(os.path.join(os.path.dirname(__file__), "VERSION"), "r").read()
except:
    __version__ = "N/A"

__all__ = ('__version__', '__main__')
